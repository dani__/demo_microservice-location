package com.demo.location;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Configuration
@RestController
@EnableAutoConfiguration
@EnableEurekaClient
@EnableCircuitBreaker
public class LocationApp 
{
    @RequestMapping("/")
    String home() {
        return "latitud:2,23234 longitud:45.3222 ";
    }
 
    public static void main(String[] args) throws Exception {
        SpringApplication.run(LocationApp.class, args);
    }
}
